class Spacing {
  static const double PagePaddingHorizontal = medium;
  static const double PagePaddingVertical = large;

  static const double xsmall = 4;
  static const double small = 8;
  static const double regular = 12;
  static const double medium = 16;
  static const double large = 24;
  static const double xlarge = 32;
  static const double xxlarge = 48;
  static const double xxxlarge = 64;
}