import 'package:flutter/material.dart';

class TextStyles {

  static const TextStyle appBarTitle = const TextStyle(
    color: Colors.white,
    fontSize: 18,
    fontWeight: FontWeight.w700,
  );

}
