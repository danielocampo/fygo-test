import 'package:flutter/material.dart';
import 'package:fygo_test/styles/spacing.dart';

enum PasswordStrength {
  NONE, WEAK, MEDIUM, STRONG
}

class _PasswordStrengthUiState {
  final List<Color> strengthBarColor;
  final Color strengthTextColor;
  final String passwordStrengthText;

  _PasswordStrengthUiState(this.strengthBarColor, this.strengthTextColor, this.passwordStrengthText);
}

class CustomPasswordField extends StatefulWidget {

  final ValueChanged<String> passwordTextChanged;
  final PasswordStrength passwordStrength;
  final TextEditingController passwordController;

  const CustomPasswordField({Key key, @required this.passwordController, this.passwordTextChanged, this.passwordStrength}) : super(key: key);

  @override
  State<StatefulWidget> createState() => _CustomPasswordFieldState();

}

class _CustomPasswordFieldState extends State<CustomPasswordField> {

  static const Color DefaultColor = Colors.black;
  static const Color WeakColor = Colors.red;
  static const Color MediumColor = Colors.blue;
  static const Color StrongColor = Colors.green;

  static const List<Color> DefaultBarGradient = const [Colors.grey, Colors.black];
  static const List<Color> WeakBarGradient = const [Colors.red, Colors.yellow];
  static const List<Color> MediumBarGradient = const [Colors.yellow, Colors.blue];
  static const List<Color> StrongBarGradient = const [Colors.blue, Colors.green];

  var _strengths = {
    PasswordStrength.NONE: _PasswordStrengthUiState(DefaultBarGradient, DefaultColor, ''),
    PasswordStrength.WEAK: _PasswordStrengthUiState(WeakBarGradient, WeakColor, 'Weak password'),
    PasswordStrength.MEDIUM: _PasswordStrengthUiState(MediumBarGradient, MediumColor, 'Medium password'),
    PasswordStrength.STRONG: _PasswordStrengthUiState(StrongBarGradient, StrongColor, 'Strong password'),
  };

  _passwordTextChanged(String newPassword) {
    widget.passwordTextChanged(newPassword);
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        TextFormField(
          controller: widget.passwordController,
          obscureText: true,
          obscuringCharacter: '*',
          decoration: InputDecoration(
            labelText: "Password",
            focusedBorder: UnderlineInputBorder(
              borderSide: BorderSide(color: Colors.transparent),
            ),
            enabledBorder: UnderlineInputBorder(
              borderSide: BorderSide(color: Colors.transparent),
            ),
            suffixIcon: Visibility(
                visible: widget.passwordStrength == PasswordStrength.STRONG,
                child: Icon(Icons.check_circle, color: StrongColor,)
            )
          ),
          onChanged: _passwordTextChanged,
        ),
        Container(
          height: 2,
          decoration: BoxDecoration(
            gradient: LinearGradient(
                colors: _strengths[widget.passwordStrength].strengthBarColor
            ),
          ),
        ),
        Container(
          width: double.infinity,
          child: Padding(
            padding: EdgeInsets.only(top: Spacing.small),
            child: Text(_strengths[widget.passwordStrength].passwordStrengthText,
              style: TextStyle(
                color: _strengths[widget.passwordStrength].strengthTextColor
              ),
              textAlign: TextAlign.left,
            ),
          ),
        ),
      ],
    );
  }

}