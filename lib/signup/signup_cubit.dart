import 'dart:math';

import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:fygo_test/signup/signup_state.dart';

abstract class SignUpCubit extends Cubit<SignUpState> {
  SignUpCubit(SignUpState state) : super(state);

  void signUp(String email, String password);
}

// Simple Cubit that uses a random Sign Up for the test purposes
// In a real-life cubit, we would add a sign_up_cubit_test using bloc_test to test that the different states are emitted correctly.
class SignUpCubitImpl extends SignUpCubit {
  SignUpCubitImpl(SignUpState state) : super(state);

  @override
  void signUp(String email, String password) async {
    emit(SignUpState(status: Status.LOADING));
    bool success = false;

    try {
      success = await _performMockSignup(email, password);
    } catch (Exception) {
      print('An error occurred - Log and report the exception');
    }

    emit(SignUpState(status: success ? Status.SUCCESS : Status.ERROR));
  }

  // In a real app, this would be done in a Repository class that would abstract the way the Sign Up is done.
  // This repo would be responsible for managing the sign up process with the backend.
  Future<bool> _performMockSignup(String email, String password) async {
    await Future.delayed(const Duration(seconds: 1), (){});

    return Random().nextBool();
  }

}

