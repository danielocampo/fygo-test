import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:fygo_test/components/custom_password_field.dart';
import 'package:fygo_test/signup/signup_cubit.dart';
import 'package:fygo_test/signup/signup_state.dart';
import 'package:fygo_test/styles/spacing.dart';
import 'package:fygo_test/styles/text_styles.dart';

class SignUpScreen extends StatefulWidget {

  @override
  State<StatefulWidget> createState() => _SignUpScreenState();

}

class _SignUpScreenState extends State<SignUpScreen> {
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();

  TextEditingController _emailController = TextEditingController(text: '');
  TextEditingController _passwordController = TextEditingController(text: '');
  FocusNode _emailFocusNode = FocusNode();

  String _password;
  PasswordStrength _passwordStrength;

  @override
  void initState() {
    super.initState();

    _passwordStrength = PasswordStrength.NONE;
  }

  _passwordTextChanged(String newPassword) {
    setState(() {
      _password = newPassword;
      if (newPassword.isEmpty) {
        _passwordStrength = PasswordStrength.NONE;
      } else if (newPassword.length < 4) {
        _passwordStrength = PasswordStrength.WEAK;
      } else if (newPassword.length < 7) {
        _passwordStrength = PasswordStrength.MEDIUM;
      } else {
        _passwordStrength = PasswordStrength.STRONG;
      }
    });
  }

  _signUpButtonPressed() {
    if (_formKey.currentState.validate()) {
      BlocProvider.of<SignUpCubit>(context).signUp(_emailController.text, _password);
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      appBar: AppBar(
        title: Text(
          "Sign Up",
          style: TextStyles.appBarTitle,
        ),
      ),
      body: BlocListener<SignUpCubit, SignUpState>(
        listener: (context, state) {
          if (state.status == Status.ERROR) {
            _buildSnackbar("There was an error, please try again.");
            return;
          }

          if (state.status == Status.SUCCESS) {
            _buildSnackbar(
              "You are a new member of Fygo!",
              backgroundColor: Colors.green,
            );
            return;
          }
        },
        child: BlocBuilder<SignUpCubit, SignUpState>(
          builder: (context, state) {
            // Here we would check for different states and build the appropriate widget.
            // In this case, there's just one state with different statuses that are only rendered through Snackbars, so we can
            // handle this in the bloc listener and only display a loading bar when needed.
            return SafeArea(
              child: Padding(
                padding: const EdgeInsets.symmetric(horizontal: Spacing.PagePaddingHorizontal, vertical: Spacing.PagePaddingVertical),
                child: state.status == Status.LOADING ? Center(child: CircularProgressIndicator()) :
                Column(
                  children: [
                    Expanded(
                        child: _signUpFormWidget()
                    ),
                    FlatButton(
                      child: Text(
                          "Sign Up"
                      ),
                      onPressed: _signUpButtonPressed,
                    )
                  ],
                ),
              ),
            );
          }
        ),
      ),
    );
  }

  Widget _signUpFormWidget() {
    return Form(
      key: _formKey,
      child: Column(
        children: [
          Text(
            "Enter your username and password to register a new user. Make sure to use a strong password for security reasons."
          ),
          SizedBox(height: Spacing.medium,),
          TextFormField(
            controller: _emailController,
            focusNode: _emailFocusNode,
            decoration: InputDecoration(
              labelText: "Email address",
              hintText: "daniel@fygo.com"
            ),
            validator: (value) => value.isEmpty ? 'This field is mandatory' : null,
            keyboardType: TextInputType.emailAddress,
            onEditingComplete: () => _emailFocusNode.nextFocus(),
          ),
          SizedBox(height: Spacing.medium,),
          CustomPasswordField(
            passwordController: _passwordController,
            passwordStrength: _passwordStrength,
            passwordTextChanged: _passwordTextChanged,
          )
        ],
      ),
    );
  }

  ScaffoldFeatureController _buildSnackbar(message, {Color backgroundColor = Colors.red}) {
    return _scaffoldKey.currentState.showSnackBar(
      SnackBar(
        content: Text(message),
        backgroundColor: backgroundColor,
        duration: Duration(seconds: 3),
      ),
    );
  }

}