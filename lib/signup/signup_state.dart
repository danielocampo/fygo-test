enum Status {
  INIT, LOADING, SUCCESS, ERROR
}

// This is the simplest state. Normally, for more complex States we could have an abstract class representing the screen state
// and have child states representing the different states this screen can take.
// e.g: if the Registration process had a step where a confirmation code had to be entered, we could have two possible SignUpState:
// SignUpUserState and ConfirmCodeState.
// This way is easier and more readable to react to states in screens, in the form of `if (state is ConfirmCodeState)...`
class SignUpState {
  Status status;

  SignUpState({this.status});
}