# fygo_test

Fygo test project

## Decisions and structure

Given it is a simple app, it has a pretty straightforward structure. Besides the CustomPasswordField, I decided to include a
very simple Cubit just to portray the architecture I am lately choosing for my Flutter apps. It should contain all the business logic
related to the Sign Up process and delegate into repositories the actual sign up.

As per the Custom password field itself, I created a custom component using a TextFormField that reacts to changes to the password strength property.
The key decisions here were:
- The strength bar is rendered in a separate Container, because it is needed to draw a gradient on it. It was indeed possible to just use
the focusedBorder property to change the color of the underline in the TextFormField, but in this way we wouldn't be able to use gradients (afaik).
- The password strength is provided by the consumer of this widget, to allow the customisation of the Strength Algorithm. The component should not
really have the responsibility of deciding what is or what isn't a strong password.
- This could be done in different ways, like using a Strategy pattern or alike, but I chose to follow the pattern that is used for Flutter components.
This is, to include a property that the parent can change and the component would react to.

The SignUp screen is simple enough as to not extract any components out of it right now, but I wouldn't keep it too large to make it more readable.

Regarding State management, I describe briefly in the code how I normally use the Bloc library to have clear and easy to consume States of each screen.

*What is missing*
As it is a test project, it is missing 2 important things: a DI module (I am using lately the GetIt lib) and Unit Tests for the business logic (bloc_test).
There should be also more validation around both fields, like being a valid email. I only included a simple example to validate the email field not being empty, but 
validation of TextFormFields can easily be done through the validators either on submit or as the user types.



